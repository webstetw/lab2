#include "main.h"
#include "master.cc"
#include "slave.cc"
#include <stdio.h>
#include <unistd.h>
/* CS_452 - Operating Systems:  Lab #2
 *  Authors: Tim Webster
 *            -Calvin
 *  Purpose - Use TCP to perfrom a Erathosthenes sieve and
 *  retrieve the list of all prime numbers.
 *
 *  The list of primes and remaining numbers will be sent between two systems.
 *  The list will be sent after the completion of a single sieve.
 *
 *  We first sieve out all multiples of 2, then 3, then 5, and so forth up until
 *  we arrive at a number <= sqrt(n), where n is the upper limit to stop looking for primes.
 *
 *  At each increment, all numbers up to n^2 will already be crossed off, so we begin at n^2.
 *  With each new sieve level.
 *
 *  If the number of remaining primes is small, i.e. <= 10, print out all primes.
 *  If the number of remaining primes is large, i.e. > 10, print out first 5 and last 5 primes.
 *
 */



int main(int argc, char **args){

  //If the args d was added, print out all debug info
/*  if(args[1] != NULL){
    if(args[1][0] == 'd'){
      _debug = 1;
    }
  }
*/
 
  bool master = false;
  bool designated = 0;
  char option;
  string num = "";
  string usagemsg = "usage:\n\tSlave: ./run -s [-e]\n\tMaster: ./run -m [-n number] [-edt]\n\t -e for debug\n\t -h for help message\n\t -d enable longer delay\n\t -t delay_time for added delay in usleep calls\n\n";
  //Maybe add option for port/ip of master/slave
  //e.g. s:
  while((option = getopt(argc, args, "emsn:dt:h")) != EOF){
    switch(option){
    case 'e':
      _debug = 1;
      break;
    case 'm':
      designated++;
      master=true;
      break;     
    case 's':
      designated++;
      master=false;
      break;
    case 'n':
      num = optarg;
      break;
    case 'd':
      _delay = 1;
      break;
    case 't':
      delay_time = atoi(optarg);
      break;
    case 'h':
      printf("%s\n",usagemsg.c_str());
      exit(0);
      break;
    default:
      printf("%s\n",usagemsg.c_str());
      exit(0);
      break;
    }
  }

  
if(designated==2){
      cout << "The -m and -s flags are mutually exclusive" << endl;
      return -1;

  }


  if(designated==0){
    string input = "";
    cout << "Please enter 0 to start program as master, 1 as slave" << endl;
    getline(cin, input);

    if(input == "0"){
      master = true;
    }
  }


  if(master){
    init_master(num);

  }else{
    init_slave();
  }


  return 0;
}
