/*
   client.cpp

   Test client for the tcpsockets classes. 

   ------------------------------------------

   Copyright � 2013 [Vic Hargrave - http://vichargrave.com]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>
#include "tcpconnector.h"


using namespace std;

int main(int argc, char** argv)
{
  if (argc != 3) {
    printf("usage: %s <port> <ip>\n", argv[0]);
    exit(1);
  }

  int len;
  string message;
  char line[BUFSIZ];
  TCPConnector* connector = new TCPConnector();
  TCPStream* stream = connector->connect(argv[2], atoi(argv[1]));
  if (stream) {
    message = "ns";
    stream->send(message.c_str(), message.size());
    printf("sent - %s\n", message.c_str());
    len = stream->receive(line, sizeof(line));
    line[len] = 0;
    printf("received - %s\n", line);
    delete stream;
  }

  /*stream = connector->connect(argv[2], atoi(argv[1]));
  if (stream) {
    
    printf("sent - %s\n", message.c_str());
    len = stream->receive(line, sizeof(line));
    line[len] = 0;
    printf("received - %s\n", line);
    delete stream;
    }*/


  int i;
  stringstream ss;
  for(i=0;i<100;i++){
    stream = connector->connect(argv[2], atoi(argv[1]));
    ss << i << " ";
    string message = ss.str();
    cout << message << endl;
    stream->send(message.c_str(), message.size());
    
    //ss.str("");
    delete stream;
  }
  
  exit(0);
}
