#ifndef _MAIN_H
#define _MAIN_H

#include <stdlib.h>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <algorithm>
#include <cstdbool>
#include <sstream>
#include <vector>
#include <string>
#include "tcpconnector.h"
#include "tcpacceptor.h"
#include <math.h>


typedef unsigned long int number;

using namespace std;
using namespace boost;

int _debug = 0;
int _delay = 0;
int delay_time = 0;


const char *master_ip = "137.28.8.167"; // thing-04.cs.uwec.edu
const char *slave_ip = "137.28.8.168"; // thing-05.cs.uwec.edu
const char *port = "9143"; // port we will be using for sending sieves
//const char *port = "9143"; // port we will be using for sending checks

typedef std::vector<int> sieve;
typedef std::vector<sieve> sieves;
typedef vector<string> strings;

typedef enum {MASTER, SLAVE} SENDTO;

void debug(string s);
void print_sieve(sieve &s, bool send);


//  fucntion to use to sort out the final vector containing all primes
bool mysort (int i, int j) { return (i<j);}

//  master_array is a vector of sieve vectors created by the master program (A)   
//  slave_array is a vector of sieve vectors created by the slave program (B)
//  
//  After the last sieve has been completed, we need to combine each prime 
//  (located in slave_array[i][0]) into a new array making up all of the primes
//  found by the slave (program b).
//  After receiving this list, we can combine each prime from the master
//  (located in master_array[i][0]) and then finally combine the two lists.
//  This will produce a single list of all of the prime numbers obtained.
//  
//  We can either combine the lists and then perform a sort afterwards.
//  Or, I believe it will save some time if we go through both the 
//  master&slave arrays at the same time, first adding from 
//  master_array[i][0] and then slave_array[i][0].  (This should work
//  since we obtained the first prime in the master list, and then
//  switched from master to slave then slave to master for each subsequent run. 
// 

sieve primes;
sieve slave_primes;
sieve master_primes;


//join two sieves
void join_sieves(sieve &s1, sieve &s2){
  for(auto i = s2.begin(); i != s2.end(); ++i){
    s1.push_back(*i);
  }
}


bool is_number(const std::string& s)
{
  std::string::const_iterator it = s.begin();
  while (it != s.end() && std::isdigit(*it)) ++it;
  return !s.empty() && it == s.end();
}

//  join_primes
//  This procedure takes the main two lists of primes generated and puts them
//  together in a sorted order.
//
//  The two sieves of prime numbers being joined are the primes found by the master
//  located in: in master_primes, and the primes found by the slave, located in: slave_primes
//
void join_primes(sieve &m_primes, sieve &s_primes, sieve &joined_primes){
  int m_size = m_primes.size() -1;
  int s_size = s_primes.size() -1;
  int count = 0;
  int skew = std::abs(m_size - s_size);
  int stop = 0;
  if(m_size == s_size){
    stop = m_size;
    debug("master_primes and slave_primes are equal in size - last sieve on slave");
  }
  if(m_size > s_size){
    stop = s_size;
  }
  if(s_size > m_size){
    stop = m_size;
  }

  for(count = 0; count <= stop; count++){
    joined_primes.push_back(m_primes.at(count));
    joined_primes.push_back(s_primes.at(count));
  }
  if(m_size > s_size){
    joined_primes.push_back(m_primes.at(count));
    debug("master_primes had +1 compared to slave_primes - last sieve on master");
  }
  if(s_size > m_size){
    joined_primes.push_back(s_primes.at(count));
    //debug("slave_primes had +1 compared to master_primes - last sieve on slave");
  }
}



//  wait_for_response
//
//
void wait_for_response(TCPStream *stream){
  ssize_t len;
  char line[BUFSIZ];
  stringstream ss;
  string message;
  bool done = false;
 
  //usleep(100);
  while(!done){
    debug("in wait for response");
      
    if((len = stream->receive(line, sizeof(line))) > 0){   
      ss << line;
      message = ss.str();
      if(message.find("|") != std::string::npos){
	debug("in wait for response - response found");
	done = true;
	return;
      }
      ss.str("");
    }
  }
}


//  recv_check
//  notify the sender that the message has arrived
//
//
void recv_check(TCPStream *stream){
  string message = " |||||||| ";
  stream->send(message.c_str(), BUFSIZ);
}


//  send_sieve
//  
//
void send_sieve(sieve &s, TCPStream *stream, string type){
  
  std::sort (s.begin(), s.end(), mysort);
  s.erase(unique(s.begin(),s.end()), s.end());
  
  print_sieve(s, true);
  char line[BUFSIZ];
  string message;
  stringstream ss;
  int i = 0;
  ss << type;
  
  for(i = 0;i<s.size();i++){
    ss << "a" << s.at(i) << " ";
    if((i+1) % 100 == 0){
      message = ss.str();
      stream->send(message.c_str(), BUFSIZ);
      if(s.size() < 100){
	usleep(600+delay_time);
      }else if(s.size() < 10000){
	if(_delay){
	  usleep(1500+delay_time);
	}else{
	  usleep(800+delay_time);
	}
      }else{
	if(_delay){
	  usleep(2200+delay_time);
	}else{
	  usleep(1500+delay_time);
	}
      }      
      
      if(_debug)cout << " line sent  " << message << endl;
  
      ss.str("");
      message = ss.str();
    }   
  }
 
  ss << " es es es es es es es es es es es es es es es";
  message = ss.str();
  
  stream->send(message.c_str(), BUFSIZ);
  
  if(_debug)cout << " last line  " << message << endl;
   
  ss.str("");
  message = ss.str();
}

//  found_check
//
//
bool found_check(string message, char* needle){
  if(message.find(needle) != std::string::npos){
    return true;
  }
  
  return false;
}


bool contains(sieve &s, int num){
  if(std::find(s.begin(), s.end(), num) != s.end()){
    return true;
  }
  return false;
}



int recv_sieve(sieve &s, TCPStream *stream){
  bool sieveRec = false;
  bool fullyRec = false;
  ssize_t len;
  char line[BUFSIZ];
  strings string_list;
  int result = 0;
  bool started = false;
  
  cout << "waiting to recv new sieve" << endl;

  while(!fullyRec){
    if((len = stream->receive(line, sizeof(line)) > 0)){
      //      cout << line << endl;
      if(line[0] != '\n'){
	//	recv_check(stream);
	stringstream ss;
	string message;
	ss << line;
	message = ss.str();
	
	if(found_check(message, "ns") || found_check(message,"spl")){
	  started = true;
	}
	if(started){
	if(!found_check(message, "spl")){
	  string_list.push_back(message);
	}else{
	  string s = "spl";
	  string_list.push_back(s);
	  break;
	}
	 
	if(_debug)cout << "recv line  " << message << endl;
	if(found_check(message, "es") || found_check(message, "spl")){
	  fullyRec = true;
	}
      }
      }
    }
  }
  
  int l;
  //  bool started = false; 
  for(l = 0;l < string_list.size();l++){
    
    std::vector<std::string> parts;
    trim(string_list.at(l));
    boost::algorithm::split(parts, string_list.at(l), boost::is_any_of(" "));
    
    if(_debug)cout << "parsing through " << string_list.at(l) << endl;
    int i;
    for(i=0;i<parts.size();i++){
      if((parts.at(i)).find("ns") != std::string::npos || parts.at(i).find('\n') != std::string::npos || (parts.at(i).length() <2)){
	continue;
      }else if(parts.at(i).find("|") != std::string::npos){
	//      cout << line << endl;
	stream->send(" | ", 32);
	debug("found check - found |");
      }else if((parts.at(i)).find("sls") != std::string::npos){
	result = 1;
	goto stop;
      }else if((parts.at(i)).find("spl") != std::string::npos){
	debug("\n\n asked for final slaves prime list \n\n");
	result = 2;
	goto stop;
      }else if((parts.at(i)).find("es") != std::string::npos){
	debug("current set of numbers ended");
	sieveRec = true;
	goto stop;
	
      }else if(parts.at(i)[0] == 'a'){
	erase_all(parts.at(i), "a");
	if(is_number(parts.at(i))){
	  //	  debug("added a number");
	  s.push_back(std::stoi(parts.at(i)));
	  // debug(parts.at(i));
	}
      } 
    
    }
  }
 stop: {
  if(result ==0)print_sieve(s, false);
  return result;
  }
  return result;
}


// receive_seive
//
// result 0 = regular sieve
// result 1 = send last sieve
// result 2 = send prime list
//
int receive_sieve(sieve &s, TCPStream *stream){  
  bool sieveRec = false;
  int result = 0;
  while(stream != NULL){ 
    
    ssize_t len;
    char line[BUFSIZ]; 
     
    if((len = stream->receive(line, sizeof(line)) > 0)){
      //      cout << line << endl;
      stringstream ss;
      string message;
      ss << line;
      message = ss.str();
      
      
      
      //if(message.find("ns") != std::string::npos || message.find("spl") != std::string::npos || message.find("|") != std::string::npos){
      if(found_check(message,"ns") || found_check(message,"spl") || found_check(message,"|")){
      debug("starting to get a new set of #'s");
      debug(message.c_str());
      
	std::vector<std::string> parts;
	boost::algorithm::split(parts, message, boost::is_any_of(" "));
	while(!sieveRec){
	  	  
	  int i;
	  for(i=0;i<parts.size();i++){
	    if((parts.at(i)).find("ns") != std::string::npos || parts.at(i).find('\n') != std::string::npos){
	      continue;
	    }else if(parts.at(i).find("|") != std::string::npos){
	      //      cout << line << endl;
	            stream->send(" | ", 32);
	        debug("found check - found |");
	    }else if((parts.at(i)).find("sls") != std::string::npos){
	      result = 1;
	      goto stop;
	    }else if((parts.at(i)).find("spl") != std::string::npos){
	      debug("\n\n asked for final slaves prime list \n\n");
	      result = 2;
	      goto stop;
	    }else if((parts.at(i)).find("es") != std::string::npos){
	      debug("current set of numbers ended");
	      sieveRec = true;
	      goto stop;
	      
	    }else if(std::isdigit(parts.at(i)[0])){
	      //	      if(!contains(s, std::stoi(parts.at(i)))){
	      debug("added a number");
	      s.push_back(std::stoi(parts.at(i)));
	      debug(parts.at(i));
	      }
	    // }
	   
	  }
	  ss.str("");
	  stream->receive(line, sizeof(line));
	  ss << line;
	  message = ss.str();
	  
	  //parts.clear();
	  parts.erase(parts.begin(), parts.begin()+parts.size());

	  boost::algorithm::split(parts, message, boost::is_any_of(" "));
	}
      }
    }
  }
 stop: 
  if(result ==0)print_sieve(s, false);
  return result;
}





//  Debug procedure prints out a debug information if 'd' was included
//  as an argument when the program was initiated.   
void debug(string s){
  if(_debug){
    cout << "debug info: " << s << endl;
  }
}



//  Print the full contents of the sieve if it has 10 or less primes
//  Otherwise, if it contains more than 10 items, print only the first
//  5 and the last 5 items.
void print_sieve(sieve &s){
  int count = 0;
  if(s.size() < 11){
    for (auto i = s.begin(); i != s.end(); ++i){
      std::cout << *i <<  (count == s.size()-1 ? " " : ", ");  
      count ++;
    }
    cout << endl;
  }else{
    int count = 0;
    for(auto i = s.begin(); i != s.end(); ++i){
      if(count == 4)
	continue;
      
      std::cout << *i << (count <= 2 ? ", " : " ...\n"); 
      count++;
    }
    
    count = 0;
    for(auto i = s.rbegin(); i != s.rend(); ++i){
      if(count == 4)
	continue;
      
      std::cout << *i << (count <= 2 ? ", " : " ...\n"); 
      count++; 
    }
  }
}


//  Print the entire contents of a sieve...
//
//
void print_sieve_full(sieve &s){
  for(auto i = s.begin(); i != s.end();++i){
    std::cout << *i << " ";
  }
  cout << endl;
}

//  If the sent parameter is true, we have just received numbers and 
//  if it is false, we have just sent numbers.
//  Regardless, we will print out the first 4 numbers associated with the send/receive 
void print_sieve(sieve &s, bool sent){
  string type = (sent ? "Sent: " : "Recvd: ");
  std::cout << type;
  int count = 0;
  for(auto i = s.begin(); i!= s.end(); ++i){
    if(count == 4)
      continue;
  
    std::cout << *i << (count <= 2 ? ", " : " ...\n");
    count++;
  }
  cout << endl;
}




sieve process_sieve(sieve &s, sieve &primes){
  int num = s.at(0);
  sieve new_sieve;
  int started = 0;
  primes.push_back(num);
  for(auto i = s.begin(); i != s.end(); ++i){  
    if(*i % num != 0 && *i != num){
	new_sieve.push_back(*i);
      }
  }
  return new_sieve;
}




#endif
 
