
//sieves slave_array
//sieve slave_primes

void init_slave(){
  //Program is running as slave on this computer
  debug("running as slave - waiting for initial connection.\n");

  TCPConnector *connector = new TCPConnector();
  TCPStream* stream = connector->connect(master_ip, atoi(port));
  
  bool finished = false;
  bool connected = false;
  
  sieve s_primes;
  
  if(stream == NULL){
    printf("Master must be initiated first, exiting program...");
    exit(-1);
  }
  
  char line[BUFSIZ];    
  line[0] = '1';
  
  if(stream != NULL){
    while(!connected){
      stream->send(line, sizeof(line));
      ssize_t len;
      char line[BUFSIZ];
      if((len = stream->receive(line, sizeof(line))) > 0){
	debug("Connected to master");    
	connected = true;
      }
    }
  }
  
  string ns = "ns ";
  
  int result = 0;
  
  //sieve current_sieve;
  while(!finished){    
    sieve current_sieve;
    //result = receive_sieve(current_sieve, stream);
    result = recv_sieve(current_sieve, stream);
    switch(result){
    case 0:
      debug("processing and sending current sieve to master");
      current_sieve = process_sieve(current_sieve, s_primes);
      send_sieve(current_sieve, stream, ns);
      break;
    case 1: 
      debug("sending final sieve to master");
      send_sieve(s_primes, stream, ns);
      finished = true;
      goto stop;
      break;
    case 2:
      debug("sending slave prime list");
      send_sieve(s_primes, stream, ns);
      finished = true;
      goto stop;
      break;
    }
    // current_sieve.erase(current_sieve.begin(), current_sieve.begin()+current_sieve.size());
  }
  
 stop:
  exit(0);
  
}


