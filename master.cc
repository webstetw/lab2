
string input = "";
number max_range;       //Highest number to look for primes 
double search_cieling;  //We must search for primes equal to every value up to the search_cieling - sqrt(max_range) 
bool done;

void init_master(string num){
  //Program is running as master on this computer    
  debug("running as master");
  
  if(num.length() == 0){
    cout << "Please enter the number, n, in which to find all primes up to..." << endl;
    getline(cin, input);
  }else{
    input = num;
  }
  
  stringstream(input) >> max_range;
  search_cieling = floor(sqrt(max_range)) ;
      
  debug("Searching for all primes up to: " + input);
  char cieling[100];
  sprintf(cieling, "%lf", search_cieling);
  debug("Search cieling = ");
  debug(cieling);


  //Filter out all multiples of 2 before starting the sieve pipeline
  int i;
  master_primes.push_back(2);
  
  sieve s2;
  for(i=3;i<=max_range;i+=2) {
    s2.push_back(i); 
  }
    
  done = false;
  
  TCPStream *stream = NULL;
  TCPAcceptor *acceptor = NULL;
  acceptor = new TCPAcceptor(atoi(port));
  bool connected = false;
  
    
  //Start of master's main code
  //////////////////////////////////////////////////////////////  
  while(!connected){
    if(acceptor->start() == 0){
      stream = acceptor->accept();
      if(stream != NULL){
	ssize_t len;
	char line[BUFSIZ];
	if((len = stream->receive(line, sizeof(line))) > 0){
	  debug("Connected to slave");
	  stream->send(line, sizeof(line));
	  connected = true;
	}
      }
    }  
  }
  
  debug("onto next code");
   
  int loc = 2;
  int result = 0;

  string ns = "ns ";
  string ask_for_primes = "spl ";
  
  
  while(!done){
   
    if(loc <= search_cieling){
      send_sieve(s2, stream, ns);
      s2.erase(s2.begin(), s2.begin()+s2.size());
      
      //receive_sieve(s2, stream);
      recv_sieve(s2, stream);
      //usleep(1000);
      loc = s2.at(0);
    }
    if(loc <= search_cieling){
      s2 = process_sieve(s2, master_primes);
      loc = s2.at(0);
    }
    if(loc > search_cieling){
      //ask slave for prime list
      
      stream->send(ask_for_primes.c_str(), ask_for_primes.size());
      // cout << "asked for " << ask_for_primes.c_str() << endl; 
      // receive_sieve(slave_primes, stream);
      recv_sieve(slave_primes, stream);

      join_sieves(primes, master_primes);
      join_sieves(primes, slave_primes);
      join_sieves(primes, s2);
      
      //sort the list of primes
      std::sort (primes.begin(), primes.end(), mysort);
      // primes.erase(unique(primes.begin(),primes.end()), primes.end());

      done = true;
    }
    
  }

  /*  printf("s2 list\n\n");
  print_sieve_full(s2);

  printf("master_primes list\n\n");
  print_sieve_full(master_primes);
  
  printf("slave_primes list\n\n");
  print_sieve_full(slave_primes);
  
  printf("all primes\n\n");
   print_sieve_full(primes);
  */


  printf("\nFinal list of primes\n");
  print_sieve(primes);


  
}
