## Makefile                                                                     
CC=g++    
CFLAGS=-g -Wall -std=gnu++0x  
LIBS=-lpthread -L/usr/exec64/

HFILES=	main.h tcpstream.h tcpacceptor.h tcpconnector.h

CCFILES= $(HFILES:.h=.cc) 

OFILES= $(HFILES:.h=.o) 

##rules

all:	link

.cc.o:	$(CCFILES) $(HFILES)   
	$(CC) $(CFLAGS) -c $*.cc 

link:	$(OFILES)
	$(CC) $(CFLAGS) $(LIBS) -o run $(OFILES)  

clean:
	rm -f *.o *.*~ run